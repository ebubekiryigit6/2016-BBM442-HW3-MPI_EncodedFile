cmake_minimum_required(VERSION 3.6)
project(2016_BBM442_HW3_MPI_EncodedFile)

set(SOURCE_FILES main.c sergen.c)
add_executable(2016_BBM442_HW3_MPI_EncodedFile ${SOURCE_FILES})

# Require MPI for this project:
find_package(MPI REQUIRED)
set(CMAKE_CXX_COMPILE_FLAGS ${CMAKE_CXX_COMPILE_FLAGS} ${MPI_COMPILE_FLAGS})
set(CMAKE_CXX_LINK_FLAGS ${CMAKE_CXX_LINK_FLAGS} ${MPI_LINK_FLAGS})
include_directories(${MPI_INCLUDE_PATH})
target_link_libraries(2016_BBM442_HW3_MPI_EncodedFile ${MPI_LIBRARIES})