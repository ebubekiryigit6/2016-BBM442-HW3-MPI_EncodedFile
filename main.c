/**
 *
 * Author:      Ebubekir Yiğit
 *
 * File:        main.c
 *
 * Purpose:     MPI Read Encoded File Parallel
 *
 *              In our case we read encodedfile.txt and find after comma letters.
 *              And concat all letters to find secret code.
 *              But secret code can be available from all processers.
 *
 *
 * Compile:     mpicc -o main main.c  (or makefile)
 *
 * Run:         mpiexec -n 4 ./main
 *
 *              mpirun -n 4 ./main
 *
 *
 * Input:       none
 *
 * Output:      All processers prints their    <rank>  <local secret code>  <All of secret code>
 *
 *
 * Notes:
 *
 *
 *
 *
 */



#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>


#define MASTER 0


typedef struct {
    char *string;
    int size;
    int top;
}String;


void initializeString(String *string, int initialSize);
void concatString(String *string, char *newCharArray);
void findLocalSecretCode(String *myLines, String *local_secret_code);
int masterReadsFile(String *allofFile);



int main(int argc, char **argv) {

    int my_rank, comm_sz;

    MPI_Init(NULL, NULL);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    /* char count in file */
    int _charCount = 0;

    /* all of file chars */
    /* string library for auto increase size */
    /* only master reads it */
    String allOfFile;
    initializeString(&allOfFile,1000);

    /* all processer's local secret code */
    String local_secret_code;
    initializeString(&local_secret_code,8);



    /* master reads file and find char count and keeps all chars in file */
    if (my_rank == MASTER) {
        _charCount = masterReadsFile(&allOfFile);
    }


    /* char count sends to all processers by Master */
    MPI_Bcast(&_charCount,1,MPI_INT,MASTER,MPI_COMM_WORLD);



    /* All processers' local line */
    String myLines;
    initializeString(&myLines,_charCount/comm_sz + 20);

    /* lines sends from master */
    MPI_Scatter(allOfFile.string,_charCount/comm_sz,MPI_CHAR,myLines.string,_charCount/comm_sz,MPI_CHAR,MASTER,MPI_COMM_WORLD);



    /* all processers finds local secret code */
    findLocalSecretCode(&myLines,&local_secret_code);
    int len_local_code = local_secret_code.top + 1;


    String _SecretCode_;
    initializeString(&_SecretCode_,(len_local_code * (comm_sz + 1)));

    /* Send local lines every processers to every processers */
    /* Allgather combines all local code */
    MPI_Allgather(local_secret_code.string,len_local_code,MPI_CHAR,_SecretCode_.string,len_local_code,MPI_CHAR,MPI_COMM_WORLD);

    _SecretCode_.string[len_local_code * comm_sz] = '\0';



    /* print RANK -  LOCAL_CODE  -  ALL_SECRET_CODE */
    printf("\nMy_rank: %d,  My_Local_Secret_Code: %s     but...    Secret Code is: %s\n",my_rank,local_secret_code.string,_SecretCode_.string);



    MPI_Finalize();
    return 0;

}

/* gets all letters comes after comma */
void findLocalSecretCode(String *myLines, String *local_secret_code){
    int i;
    for (i = 0; myLines->string[i] != '\0'; i++){
        if (myLines->string[i] == ','){
            char temp[2];
            temp[0] = myLines->string[i+1];
            temp[1] = '\0';
            /* string size already increased */
            concatString(local_secret_code,temp);
        }
    }
}

/* reads file and keeps all chars and returns charcount */
int masterReadsFile(String *allofFile){
    int _charCount = 0;
    FILE *file = fopen("encodedfile.txt", "r");
    int c;

    while ((c = fgetc(file)) != EOF) {
        char temp[2];
        temp[0] = (char) c;
        temp[1] = '\0';
        /* string size already increased */
        concatString(allofFile, temp);
        _charCount++;
    }

    fclose(file);

    return _charCount;
}

/* string initializer */
void initializeString(String *string, int initialSize){
    string->string = (char*)malloc(initialSize * sizeof(char));
    string->string[0] = '\0';
    string->size = initialSize;
    string->top = -1;
}


/* combines two string */
/* string size is auto increased */
void concatString(String *string, char *newCharArray){

    int i;
    for (i = 0; newCharArray[i] != '\0'; i++){
        if (string->top == string->size -1){
            string->size *= 2;
            string->string = (char*)realloc(string->string,string->size * sizeof(char));
        }
        string->top++;
        string->string[string->top] = newCharArray[i];
    }

    if (string->top == string->size -1){
        string->size *= 2;
        string->string = (char*)realloc(string->string,string->size * sizeof(char));
    }

    string->string[string->top+1] = '\0';
}
